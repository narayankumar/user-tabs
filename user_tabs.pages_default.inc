<?php
/**
 * @file
 * user_tabs.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_handlers().
 */
function user_tabs_default_page_manager_handlers() {
  $export = array();

  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'user_view_panel_context';
  $handler->task = 'user_view';
  $handler->subtask = '';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'User page',
    'no_blocks' => 1,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(),
    'relationships' => array(),
  );
  $display = new panels_display();
  $display->layout = 'threecol_33_34_33_stacked';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'top' => NULL,
      'left' => NULL,
      'middle' => NULL,
      'right' => NULL,
      'bottom' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = '';
  $display->uuid = 'e46a0b39-e4b6-482e-aa09-0ff8a61bd903';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-9237e81a-9275-4a0b-84ea-e43188e1bb6f';
    $pane->panel = 'left';
    $pane->type = 'user_profile';
    $pane->subtype = 'user_profile';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'view_mode' => 'full',
      'context' => 'argument_entity_id:user_1',
      'override_title' => 1,
      'override_title_text' => '<h4>%title</h4>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array(
      'css_id' => '',
      'css_class' => 'user-panel',
    );
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '9237e81a-9275-4a0b-84ea-e43188e1bb6f';
    $display->content['new-9237e81a-9275-4a0b-84ea-e43188e1bb6f'] = $pane;
    $display->panels['left'][0] = 'new-9237e81a-9275-4a0b-84ea-e43188e1bb6f';
    $pane = new stdClass();
    $pane->pid = 'new-69129ad5-a051-415d-a587-446b2e2cada3';
    $pane->panel = 'left';
    $pane->type = 'views_panes';
    $pane->subtype = 'user_content_tab_on_user_page-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '69129ad5-a051-415d-a587-446b2e2cada3';
    $display->content['new-69129ad5-a051-415d-a587-446b2e2cada3'] = $pane;
    $display->panels['left'][1] = 'new-69129ad5-a051-415d-a587-446b2e2cada3';
    $pane = new stdClass();
    $pane->pid = 'new-13685c61-a0a0-4eb0-b6c6-0a0685390e9d';
    $pane->panel = 'middle';
    $pane->type = 'custom';
    $pane->subtype = 'custom';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
              2 => 7,
            ),
          ),
          'context' => 'logged-in-user',
          'not' => FALSE,
        ),
        1 => array(
          'name' => 'compare_users',
          'settings' => array(
            'equality' => '1',
          ),
          'context' => array(
            0 => 'argument_entity_id:user_1',
            1 => 'logged-in-user',
          ),
          'not' => FALSE,
        ),
      ),
    );
    $pane->configuration = array(
      'admin_title' => 'create directory entry link',
      'title' => '',
      'body' => '<p>+ <a href="../../directory/add">Create directory entry</a></p>',
      'format' => 'full_html',
      'substitute' => 1,
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '13685c61-a0a0-4eb0-b6c6-0a0685390e9d';
    $display->content['new-13685c61-a0a0-4eb0-b6c6-0a0685390e9d'] = $pane;
    $display->panels['middle'][0] = 'new-13685c61-a0a0-4eb0-b6c6-0a0685390e9d';
    $pane = new stdClass();
    $pane->pid = 'new-3d8e0473-0028-405b-ac81-47ed74d1ecc3';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'user_content_tab_on_user_page-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'role',
          'settings' => array(
            'rids' => array(
              0 => 3,
              1 => 4,
              2 => 5,
            ),
          ),
          'context' => 'argument_entity_id:user_1',
          'not' => TRUE,
        ),
      ),
    );
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '3d8e0473-0028-405b-ac81-47ed74d1ecc3';
    $display->content['new-3d8e0473-0028-405b-ac81-47ed74d1ecc3'] = $pane;
    $display->panels['middle'][1] = 'new-3d8e0473-0028-405b-ac81-47ed74d1ecc3';
    $pane = new stdClass();
    $pane->pid = 'new-ee337b82-3759-453f-80a5-a9b0c722c206';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'following-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = 'ee337b82-3759-453f-80a5-a9b0c722c206';
    $display->content['new-ee337b82-3759-453f-80a5-a9b0c722c206'] = $pane;
    $display->panels['middle'][2] = 'new-ee337b82-3759-453f-80a5-a9b0c722c206';
    $pane = new stdClass();
    $pane->pid = 'new-ec46d420-fef7-43ee-b86c-b6811bd30f62';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'following-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 3;
    $pane->locks = array();
    $pane->uuid = 'ec46d420-fef7-43ee-b86c-b6811bd30f62';
    $display->content['new-ec46d420-fef7-43ee-b86c-b6811bd30f62'] = $pane;
    $display->panels['middle'][3] = 'new-ec46d420-fef7-43ee-b86c-b6811bd30f62';
    $pane = new stdClass();
    $pane->pid = 'new-05c13eea-7a97-45cc-89b6-4819c33c11d2';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'user_ratings_on_user_page-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 4;
    $pane->locks = array();
    $pane->uuid = '05c13eea-7a97-45cc-89b6-4819c33c11d2';
    $display->content['new-05c13eea-7a97-45cc-89b6-4819c33c11d2'] = $pane;
    $display->panels['middle'][4] = 'new-05c13eea-7a97-45cc-89b6-4819c33c11d2';
    $pane = new stdClass();
    $pane->pid = 'new-9458de90-f1f6-4d6a-aa5c-5e3ad0ed52e3';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'comments_tab_on_user_page-panel_pane_2';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 5;
    $pane->locks = array();
    $pane->uuid = '9458de90-f1f6-4d6a-aa5c-5e3ad0ed52e3';
    $display->content['new-9458de90-f1f6-4d6a-aa5c-5e3ad0ed52e3'] = $pane;
    $display->panels['middle'][5] = 'new-9458de90-f1f6-4d6a-aa5c-5e3ad0ed52e3';
    $pane = new stdClass();
    $pane->pid = 'new-4044cf97-5bb8-4a82-b5df-2e777a5b03ed';
    $pane->panel = 'middle';
    $pane->type = 'views_panes';
    $pane->subtype = 'comments_tab_on_user_page-panel_pane_1';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'context' => array(
        0 => 'argument_entity_id:user_1',
      ),
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 6;
    $pane->locks = array();
    $pane->uuid = '4044cf97-5bb8-4a82-b5df-2e777a5b03ed';
    $display->content['new-4044cf97-5bb8-4a82-b5df-2e777a5b03ed'] = $pane;
    $display->panels['middle'][6] = 'new-4044cf97-5bb8-4a82-b5df-2e777a5b03ed';
    $pane = new stdClass();
    $pane->pid = 'new-2be1fa42-ed20-42a6-826c-1567dea7f991';
    $pane->panel = 'right';
    $pane->type = 'block';
    $pane->subtype = 'statuses-statuses';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_title' => 1,
      'override_title_text' => '<h4 class="black">status</h4>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '2be1fa42-ed20-42a6-826c-1567dea7f991';
    $display->content['new-2be1fa42-ed20-42a6-826c-1567dea7f991'] = $pane;
    $display->panels['right'][0] = 'new-2be1fa42-ed20-42a6-826c-1567dea7f991';
    $pane = new stdClass();
    $pane->pid = 'new-5325eda2-34e6-43cf-a039-c06e071e38fd';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'statuses_stream';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '10',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'block_2',
      'override_title' => 1,
      'override_title_text' => '',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5325eda2-34e6-43cf-a039-c06e071e38fd';
    $display->content['new-5325eda2-34e6-43cf-a039-c06e071e38fd'] = $pane;
    $display->panels['right'][1] = 'new-5325eda2-34e6-43cf-a039-c06e071e38fd';
    $pane = new stdClass();
    $pane->pid = 'new-61480778-3dd7-4e22-864f-4360de361682';
    $pane->panel = 'right';
    $pane->type = 'views';
    $pane->subtype = 'statuses_private_messages';
    $pane->shown = TRUE;
    $pane->access = array();
    $pane->configuration = array(
      'override_pager_settings' => 0,
      'use_pager' => 0,
      'nodes_per_page' => '0',
      'pager_id' => '0',
      'offset' => '0',
      'more_link' => 0,
      'feed_icons' => 0,
      'panel_args' => 0,
      'link_to_view' => 0,
      'args' => '',
      'url' => '',
      'display' => 'default',
      'context' => array(
        0 => 'argument_entity_id:user_1.uid',
      ),
      'override_title' => 1,
      'override_title_text' => '<h4 class="black">private messages</h4>',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 2;
    $pane->locks = array();
    $pane->uuid = '61480778-3dd7-4e22-864f-4360de361682';
    $display->content['new-61480778-3dd7-4e22-864f-4360de361682'] = $pane;
    $display->panels['right'][2] = 'new-61480778-3dd7-4e22-864f-4360de361682';
  $display->hide_title = PANELS_TITLE_NONE;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $export['user_view_panel_context'] = $handler;

  return $export;
}
