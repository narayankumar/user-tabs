<?php
/**
 * @file
 * user_tabs.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function user_tabs_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "page_manager" && $api == "pages_default") {
    return array("version" => "1");
  }
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function user_tabs_views_api($module = NULL, $api = NULL) {
  return array("api" => "3.0");
}

/**
 * Implements hook_flag_default_flags().
 */
function user_tabs_flag_default_flags() {
  $flags = array();
  // Exported flag: "Abusive user flag".
  $flags['abusive_user_flag'] = array(
    'entity_type' => 'user',
    'title' => 'Abusive user flag',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Report [user:name] for abuse',
    'flag_long' => '',
    'flag_message' => '',
    'unflag_short' => 'Cancel reporting [user:name] for abuse',
    'unflag_long' => '',
    'unflag_message' => '',
    'unflag_denied_text' => '',
    'link_type' => 'confirm',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'show_on_profile' => 0,
    'access_uid' => 'others',
    'flag_confirmation' => 'Are you sure you want to report this user?',
    'unflag_confirmation' => 'Are you sure you want to cancel reporting this user?',
    'module' => 'user_tabs',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  // Exported flag: "Follow user".
  $flags['follow_user'] = array(
    'entity_type' => 'user',
    'title' => 'Follow user',
    'global' => 0,
    'types' => array(),
    'flag_short' => 'Follow [user:name]',
    'flag_long' => '',
    'flag_message' => 'Following [user:name]',
    'unflag_short' => 'Stop following [user:name]',
    'unflag_long' => '',
    'unflag_message' => 'Stopped following [user:name]',
    'unflag_denied_text' => '',
    'link_type' => 'toggle',
    'weight' => 0,
    'show_in_links' => array(
      'full' => 0,
      'diff_standard' => 0,
      'token' => 0,
    ),
    'show_as_field' => 1,
    'show_on_form' => 0,
    'access_author' => '',
    'show_contextual_link' => 0,
    'show_on_profile' => 0,
    'access_uid' => 'others',
    'module' => 'user_tabs',
    'locked' => array(
      0 => 'name',
    ),
    'api_version' => 3,
  );
  return $flags;

}
