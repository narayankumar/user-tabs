<?php
/**
 * @file
 * user_tabs.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function user_tabs_default_rules_configuration() {
  $items = array();
  $items['rules_make_new_user_directory_user'] = entity_import('rules_config', '{ "rules_make_new_user_directory_user" : {
      "LABEL" : "Make new user directory user",
      "PLUGIN" : "rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "USES VARIABLES" : { "account" : { "label" : "Account", "type" : "user" } },
      "IF" : [
        { "data_is" : { "data" : [ "account:field-directory-user" ], "value" : 1 } }
      ],
      "DO" : [
        { "list_add" : { "list" : [ "account:roles" ], "item" : "7", "unique" : 1 } }
      ]
    }
  }');
  $items['rules_register_new_user'] = entity_import('rules_config', '{ "rules_register_new_user" : {
      "LABEL" : "Register new user",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "rules" ],
      "ON" : { "user_update" : [] },
      "IF" : [ { "data_is" : { "data" : [ "account:status" ], "value" : "1" } } ],
      "DO" : [
        { "list_add" : { "list" : [ "account:roles" ], "item" : "10", "unique" : 1 } },
        { "entity_save" : { "data" : [ "account" ], "immediate" : 1 } },
        { "component_rules_make_new_user_directory_user" : { "account" : [ "account" ] } },
        { "redirect" : { "url" : "user" } }
      ]
    }
  }');
  $items['rules_report_user_abuse_to_site_admin'] = entity_import('rules_config', '{ "rules_report_user_abuse_to_site_admin" : {
      "LABEL" : "Report user abuse to site admin",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "flag", "rules" ],
      "ON" : { "flag_flagged_abusive_user_flag" : [] },
      "IF" : [
        { "flag_threshold_user" : {
            "flag" : "abusive_user_flag",
            "user" : [ "flagged-user" ],
            "number" : "1",
            "operator" : "\\u003E="
          }
        }
      ],
      "DO" : [
        { "mail" : {
            "to" : "narayankumar@me.com",
            "subject" : "[flag:title] has been triggered",
            "message" : "[flag:title] has been triggered on user [flagged-user:name] by the user [flagging-user:name].\\r\\n\\r\\nreason cited: \\u0022[flagging:field-abusive-user-reason]\\u0022\\r\\n\\r\\nyou can visit the page user [flagged-user:url] and check if anything is amiss.\\r\\n\\r\\nyou can also contact the complaining user at [flagged-user:mail]\\r\\n\\r\\n-- your friendly robot at gingertail.in ",
            "language" : [ "" ]
          }
        },
        { "redirect" : { "url" : "\\u003Cfront\\u003E" } },
        { "drupal_message" : { "message" : "A message has been sent to a site admin for suitable action." } }
      ]
    }
  }');
  return $items;
}
